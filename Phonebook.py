class Phonebook(object):
    def __init__(self):
        self.pb = {}

    def add_entry(self, name, number):
        self.pb[name] = number

    def remove_entry(self, name):
        del self.pb[name]
