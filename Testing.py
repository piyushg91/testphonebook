from Phonebook import Phonebook
import unittest

class Tester(unittest.TestCase):
    def test_creation(self):
        self.testPB = Phonebook()

    def test_adding(self):
        self.test_creation()
        self.testPB.add_entry("Piyush", 858)
        self.assertEqual(self.testPB.pb["Piyush"], 858)

    def test_deleting(self):
        self.test_adding()
        self.testPB.remove_entry("Piyush")
        self.assertTrue("Piyush" not in self.testPB.pb)

if __name__ == "__main__":
    unittest.main()
